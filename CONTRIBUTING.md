Contributing to Cinema
======================

This page documents at a very high level how to contribute to Cinema.

Setup
-----

For information about setting up a gitlab account and how to fork this project go [here][VTK_contributing] and [here][VTK_develop].

Workflow
--------

Cinema development uses a [branchy workflow][branchy] equivalent to [that one used in projects like VTK][VTK_develop], based on a master branch and different topic branches. The master branch always reflects a state with the latest delivered development changes and is the place from where topic branches are branched and merged to. It should always be in a stable state.

[branchy]: https://gitlab.kitware.com/vtk/vtk/blob/master/Documentation/dev/git/develop.md

[VTK_contributing]: https://gitlab.kitware.com/vtk/vtk/blob/master/CONTRIBUTING.md

[VTK_develop]: https://gitlab.kitware.com/vtk/vtk/blob/master/Documentation/dev/git/develop.md

Style
-----

Aim for PEP8 style compliance. flake8 should not report any violations in the library.
